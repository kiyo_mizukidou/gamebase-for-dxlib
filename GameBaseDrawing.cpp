#include "ZQGameBaseDXL.h"

/*����������������������
�@�������� �`�� ��������
�@����������������������*/

//����������W���w�聡��
void Mode::DrawNormal(int handle,int x,int y,int alpha){
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	DrawGraph(x,y,picture[handle],TRUE);
}
void Mode::DrawNormal(int handle,float x,float y,int alpha){
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	DrawGraphF(x,y,picture[handle],TRUE);
}

//�������S���W�E��]�p�E��][�O]�̊g��[��]���w�聡��
void Mode::DrawRotating(int handle,float x,float y,double theta,double xex,double yex,int alpha,bool lrturn){
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	if(xex==1.f && yex==1.f){
		DrawRotaGraphF(x,y,1.0,theta,picture[handle],TRUE,lrturn);
	}
	else{
		int w,h;
		GetGraphSize(picture[handle],&w,&h);
		DrawRotaGraph3F(x,y,w/2.f,h/2.f,xex,yex,theta,picture[handle],TRUE,lrturn);
	}
}

//�������S���W�E��]�p�E��][��]�̊g��[��]���w�聡��
void Mode::DrawGumball(int handle,float x,float y,double theta,float w,float h,int alpha,bool lrturn){
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND,255);
	int hh,ww;
	GetGraphSize(picture[handle],&ww,&hh);
	ww=ww>hh?ww:hh;
	int palette=MakeScreen(ww,ww,TRUE);
	SetDrawScreen(palette);
	DrawRotaGraphF(ww/2.f,ww/2.f,1,theta,picture[handle],TRUE,lrturn);

	SetDrawScreen(DX_SCREEN_BACK);
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	DrawExtendGraphF(x-w/2,y-h/2,x+w/2,y+h/2,palette,TRUE);

	DeleteGraph(palette);
}

//�������S���W�E�g�啝���w�聡��
void Mode::DrawSquare(int handle,float x,float y,float w,float h,int alpha){
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	DrawExtendGraphF(x-w/2,y-h/2,x+w/2,y+h/2,picture[handle],TRUE);
}

//�������S���W�E�������w�肵�ĉ~�O���t�Q�[�W�`�恡��
void Mode::DrawGauge(int handle,int x,int y,double ratio,int alpha){
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	DrawCircleGauge(x,y,100*ratio,picture[handle]);
}

/*����������������������
�@�� �`��i���L�摜�j ��
�@����������������������*/

//����������W���w��i���[�h�ԋ��L�摜�j����
void Mode::GDrawNormal(int handle,int x,int y,int alpha){
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	DrawGraph(x,y,picture[handle],TRUE);
}
void Mode::GDrawNormal(int handle,float x,float y,int alpha){
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	DrawGraphF(x,y,picture[handle],TRUE);
}

//�������S���W�E��]�p�E��][�O]�̊g��[��]���w��i���[�h�ԋ��L�摜�j����
void Mode::GDrawRotating(int handle,float x,float y,double theta,double xex,double yex,int alpha,bool lrturn){
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	if(xex==1.f && yex==1.f){
		DrawRotaGraphF(x,y,1.0,theta,picture[handle],TRUE,lrturn);
	}
	else{
		int w,h;
		GetGraphSize(picture[handle],&w,&h);
		DrawRotaGraph3F(x,y,w/2.f,h/2.f,xex,yex,theta,picture[handle],TRUE,lrturn);
	}
}

//�������S���W�E��]�p�E��][��]�̊g��[��]���w��i���[�h�ԋ��L�摜�j����
void Mode::GDrawGumball(int handle,float x,float y,double theta,float w,float h,int alpha,bool lrturn){
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND,255);
	int hh,ww;
	GetGraphSize(picture[handle],&ww,&hh);
	ww=ww>hh?ww:hh;
	int palette=MakeScreen(ww,ww,TRUE);
	SetDrawScreen(palette);
	DrawRotaGraphF(ww/2.f,ww/2.f,1,theta,picture[handle],TRUE,lrturn);

	SetDrawScreen(DX_SCREEN_BACK);
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	DrawExtendGraphF(x-w/2,y-h/2,x+w/2,y+h/2,palette,TRUE);

	DeleteGraph(palette);
}

//�������S���W�E�g�啝���w��i���[�h�ԋ��L�摜�j����
void Mode::GDrawSquare(int handle,float x,float y,float w,float h,int alpha){
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	DrawExtendGraphF(x-w/2,y-h/2,x+w/2,y+h/2,picture[handle],TRUE);
}

//�������S���W�E�������w�肵�ĉ~�O���t�Q�[�W�`��i���[�h�ԋ��L�摜�j����
void Mode::GDrawGauge(int handle,int x,int y,double ratio,int alpha){
	SetDrawBlendMode(alpha==255?DX_BLENDMODE_NOBLEND:DX_BLENDMODE_ALPHA,alpha);
	DrawCircleGauge(x,y,100*ratio,picture[handle]);
}

/*����������������������
�@������ ��ʌ��� ������
�@����������������������*/

//��������ʂ���t�F�[�h�C������
void Mode::FXBlackIn(int start,int end,int t){
	if(start<=t && t<=end){
		int h,w,b;
		GetScreenState(&h,&w,&b);
		SetDrawBlendMode(DX_BLENDMODE_ALPHA,255*(end-t)/(end-start));
		DrawBox(0,0,h,w,GetColor(0,0,0),TRUE);
	}
}
//��������ʂփt�F�[�h�A�E�g����
void Mode::FXBlackOut(int start,int end,int t){
	if(start<=t && t<=end){
		int h,w,b;
		GetScreenState(&h,&w,&b);
		SetDrawBlendMode(DX_BLENDMODE_ALPHA,255*(t-start)/(end-start));
		DrawBox(0,0,h,w,GetColor(0,0,0),TRUE);
	}
}
//��������ʂ���t�F�[�h�C������
void Mode::FXWhiteIn(int start,int end,int t){
	if(start<=t && t<=end){
		int h,w,b;
		GetScreenState(&h,&w,&b);
		SetDrawBlendMode(DX_BLENDMODE_ALPHA,255*(end-t)/(end-start));
		DrawBox(0,0,h,w,GetColor(255,255,255),TRUE);
	}
}
//��������ʂփt�F�[�h�A�E�g����
void Mode::FXWhiteOut(int start,int end,int t){
	if(start<=t && t<=end){
		int h,w,b;
		GetScreenState(&h,&w,&b);
		SetDrawBlendMode(DX_BLENDMODE_ALPHA,255*(t-start)/(end-start));
		DrawBox(0,0,h,w,GetColor(255,255,255),TRUE);
	}
}

#include "ZQGameBaseDXL.h"
#include <math.h>

/*■■■■■■■■■■■
　■■コンストラクタ■■
　■■■■■■■■■■■*/

//■■th向きの単位ヴェクトル（角度は右から下へ向かうラディアン角度）■■
XY_VECTOR::XY_VECTOR(float th){x=cos(th);y=sin(th);}
//■■要素(x,y)なるヴェクトル■■
XY_VECTOR::XY_VECTOR(float xx,float yy){x=xx;y=yy;}

/*■■■■■■■■■■■
　■■ 関数・演算子 ■■
　■■■■■■■■■■■*/

//■■向き（ラディアン角度）■■
float XY_VECTOR::GetAngle(){return x*y!=0.f?0:atan2(y,x);}
//■■【単項演算子】大きさの２乗■■
float XY_VECTOR::operator+(){return x*x+y*y;}
//■■【単項演算子】大きさ■■
float XY_VECTOR::operator*(){return sqrt(x*x+y*y);}

//■■ヴェクトル和■■
XY_VECTOR XY_VECTOR::operator+(XY_VECTOR v2){return XY_VECTOR(x+v2.x,y+v2.y);}
//■■【単項演算子】逆ヴェクトル■■
XY_VECTOR XY_VECTOR::operator-(){return XY_VECTOR(-x,-y);}
//■■スカラー倍■■
XY_VECTOR XY_VECTOR::operator*(float r){return XY_VECTOR(r*x,r*y);}
XY_VECTOR operator*(float r,XY_VECTOR v){return XY_VECTOR(v.x*r,v.y*r);}
//■■ヴェクトル差■■
XY_VECTOR XY_VECTOR::operator-(XY_VECTOR v2){return XY_VECTOR(x-v2.x,y-v2.y);}
//■■内積■■
float XY_VECTOR::operator*(XY_VECTOR v2){return x*v2.x+y*v2.y;}
//■■外積の大きさ■■
float XY_VECTOR::operator&(XY_VECTOR v2){return x*v2.y-v2.x*y;}

//■■回転■■
XY_VECTOR XY_VECTOR::operator%(float th){return XY_VECTOR(x*cos(th)-y*sin(th),x*sin(th)+y*cos(th));}

//■■論理演算子は大きさの比較■■
bool XY_VECTOR::operator<(float r){return x*x+y*y<r*r;}
bool XY_VECTOR::operator<(XY_VECTOR v){return x*x+y*y<v.x*v.x+v.y*v.y;}
bool operator<(float r,XY_VECTOR v){return r*r<v.x*v.x+v.y*v.y;}
bool XY_VECTOR::operator==(float r){return x*x+y*y==r*r;}
bool XY_VECTOR::operator==(XY_VECTOR v){return x*x+y*y==v.x*v.x+v.y*v.y;}
bool operator==(float r,XY_VECTOR v){return r*r==v.x*v.x+v.y*v.y;}
bool XY_VECTOR::operator>(float r){return x*x+y*y>r*r;}
bool XY_VECTOR::operator>(XY_VECTOR v){return x*x+y*y>v.x*v.x+v.y*v.y;}
bool operator>(float r,XY_VECTOR v){return r*r>v.x*v.x+v.y*v.y;}


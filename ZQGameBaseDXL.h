#ifndef DEJA_ZQDXL_
#define DEJA_ZQDXL_

/*■■■■■■■■■■■■■■■■■■■■■
　■　　　　　　　　　　　　　　　　　　　■
　■◆　DxGameSysetem by Kiyo.Mizukido　◆■
　■　　　　　　　　　　　　　　　　　　　■
　■―――――――――――――――――――■
　■つくったひと：瑞樹堂浄美（未来定規）　■
　■目的：DxLibゲームの汎用クラス・関数 　■
　■最終更新日時：西暦2014年03月29日　　　■
　■　　　　　　　　　　　　　　　　　　　■
　■　使用する場合は、DXライブラリの利用　■
　■　規約にも従ってください。　　　　　　■
　■　　　　　　　　　　　　　　　　　　　■
　■―――――――――――――――――――■
　■更新履歴　　　　　　　　　　　　　　　■
　■ v.140329:BaseとModeの関係まで仮組み　■
　■　　　　　　　　　　　　　　　　　　　■
　■　　　　　　　　　　　　　　　　　　　■
　■■■■■■■■■■■■■■■■■■■■■*/
#include "DxLib.h"

/*■■■■■■■■■■■
　■■定義すべき定数■■
　■■■■■■■■■■■*/

//■■nPICS：モード間共有画像メモリに読み込む最大数■■
#ifndef nPICS
#define nPICS 1024
#endif
//■■nmPICS：各モードクラスの画像メモリに読み込む最大数■■
#ifndef nmPICS
#define nmPICS 1024
#endif
//■■FPS：frame per second、処理回数毎秒■■
#ifndef FPS
#define FPS 60
#endif

/*■■■■■■■■■■■
　■■■■クラス■■■■
　■■■■■■■■■■■*/
class ZQGameBase;
class Mode;
class XY_VECTOR;
class Thing;

//■■ZQGameBase：ゲームデータの本拠地■■
class ZQGameBase{
	friend class Mode;
public:
	ZQGameBase();
	virtual ~ZQGameBase(){}
	int GameMain();//■■メイン関数■■
	float GetFPS(){return output_FPS;}//■■表示すべきFPS現在値■■

private:
	Mode* mode;
	Mode* nextMode;
	bool retourner;
	int bgm;
	int nextBGM;
	int picture[nPICS];//■■画像■■
	char keyon[256];
	int keylong[256];//■■キーの押下時間■■
	int start_FPS;
	int cnt_FPS;
	int gnt_FPS;
	int now_FPS;
	float output_FPS;
};

//■■Mode：ベース操作諸関数に■■
class Mode{
	friend class ZQGameBase;
public:
	Mode(ZQGameBase* z);
	virtual ~Mode();

protected:
	int modeCnt;
	short modeStatus;
	ZQGameBase* base;
	int picture[nmPICS];

	virtual void Run()=0;
	virtual void Render()=0;
	void toNextMode(Mode* next);//■■次のモードを指定（NULL指定で終了）■■
	void toNextBGM(int next);//■■次のBGMを指定（NULL指定で無音）■■

	int SetPic(int suffix,TCHAR* filename);//■■画像読み込み■■
	int DivPic(int suffix,int x,int y,int w,int h,TCHAR* filename);//■■複数分割画像の読み込み■■
	int GSetPic(int suffix,TCHAR* filename);//■■画像読み込み（モード間共有）■■
	int GDivPic(int suffix,int x,int y,int w,int h,TCHAR* filename);//■■複数分割画像の読み込み（モード間共有）■■
	void DrawNormal(int handle,int x,int y,int alpha=255);//■■左上座標を指定■■
	void DrawNormal(int handle,float x,float y,int alpha=255);//■■左上座標を指定■■
	void DrawRotating(int handle,float x,float y,double theta=0.f,double xex=1.f,double yex=1.f,int alpha=255,bool lrturn=false);//■■中心座標・回転角・回転[前]の拡大[率]を指定■■
	void DrawGumball(int handle,float x,float y,double theta,float w,float h,int alpha=255,bool lrturn=false);//■■中心座標・回転角・回転[後]の拡大[幅]を指定■■
	void DrawSquare(int handle,float x,float y,float w,float h,int alpha=255);//■■中心座標・拡大幅を指定■■
	void DrawGauge(int handle,int x,int y,double ratio,int alpha=255);//■■中心座標・割合を指定して円グラフゲージ描画■■
	void GDrawNormal(int handle,int x,int y,int alpha=255);//■■左上座標を指定■■
	void GDrawNormal(int handle,float x,float y,int alpha=255);//■■左上座標を指定■■
	void GDrawRotating(int handle,float x,float y,double theta=0.f,double xex=1.f,double yex=1.f,int alpha=255,bool lrturn=false);//■■中心座標・回転角・回転[前]の拡大[率]を指定■■
	void GDrawGumball(int handle,float x,float y,double theta,float w,float h,int alpha=255,bool lrturn=false);//■■中心座標・回転角・回転[後]の拡大[幅]を指定■■
	void GDrawSquare(int handle,float x,float y,float w,float h,int alpha=255);//■■中心座標・拡大幅を指定■■
	void GDrawGauge(int handle,int x,int y,double ratio,int alpha=255);//■■中心座標・割合を指定して円グラフゲージ描画■■
	void FXBlackIn(int start,int end,int t);//■■黒画面からフェードイン■■
	void FXBlackOut(int start,int end,int t);//■■黒画面へフェードアウト■■
	void FXWhiteIn(int start,int end,int t);//■■白画面からフェードイン■■
	void FXWhiteOut(int start,int end,int t);//■■白画面へフェードアウト■■
};

//■■XY_VECTOR：便利な2Dヴェクトル■■
class XY_VECTOR{
public:
	XY_VECTOR(){x=0;y=0;}//■■零ヴェクトル■■
	XY_VECTOR(float th);//■■単位ヴェクトル■■
	XY_VECTOR(float xx,float yy);//■■要素を指定したヴェクトル■■
	virtual ~XY_VECTOR(){}
	float x,y;
	float GetAngle();//■■向き（ラディアン角度）■■
	float operator+();//■■【単項演算子】大きさの２乗■■
	float operator*();//■■【単項演算子】大きさ■■

	XY_VECTOR operator+(XY_VECTOR);//■■ヴェクトル和■■
	XY_VECTOR operator-(XY_VECTOR);//■■ヴェクトル差■■
	XY_VECTOR operator-();//■■【単項演算子】逆ヴェクトル■■
	float operator*(XY_VECTOR);//■■内積■■
	XY_VECTOR operator*(float);//■■スカラー倍■■
	XY_VECTOR friend operator*(float,XY_VECTOR);//■■スカラー倍■■
	float operator&(XY_VECTOR);//■■外積の大きさ■■
	XY_VECTOR operator%(float);//■■回転■■
	bool operator<(float);//■■論理演算子は大きさの比較■■
	bool operator<(XY_VECTOR);//■■論理演算子は大きさの比較■■
	bool friend operator<(float,XY_VECTOR);//■■論理演算子は大きさの比較■■
	bool operator==(float);//■■論理演算子は大きさの比較■■
	bool operator==(XY_VECTOR);//■■論理演算子は大きさの比較■■
	bool friend operator==(float,XY_VECTOR);//■■論理演算子は大きさの比較■■
	bool operator>(float);//■■論理演算子は大きさの比較■■
	bool operator>(XY_VECTOR);//■■論理演算子は大きさの比較■■
	bool friend operator>(float,XY_VECTOR);//■■論理演算子は大きさの比較■■
};

//■■Thing：「画面に現れるもの」の根源■■
class Thing: protected XY_VECTOR{
public:
	Thing(){r=0;v=XY_VECTOR();status=0;t=0;}
	virtual ~Thing(){}
	XY_VECTOR GetXY()const{return XY_VECTOR(x,y);}
	XY_VECTOR GetVolume()const{return v;}
	float GetRange()const{return r;}
	short GetStatus()const{return status;}
	const static short DEATH=-1;//statusに対して用いる

protected:
	short status;//死亡時-1
	XY_VECTOR v;//速度
	float r;//当たり半径
	int t;
	virtual void Run(){VMove();}
	virtual void Render(){
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND,255);
		DrawBox(int(x)-5,int(y)-5,int(x)+5,int(y)+5,GetColor(255,0,0),TRUE);
	}
	virtual void Judge(){}
	void VMove(){x+=v.x;y+=v.y;}
};


#endif
#include "ZQGameBaseDXL.h"

/*■■■■■■■■■■■
　■■ ベースクラス ■■
　■■■■■■■■■■■*/
ZQGameBase::ZQGameBase(){
	mode=NULL;
	nextMode=NULL;
	retourner=false;
	bgm=-1;
	nextBGM=-1;
	for(int i=0;i<nPICS;i++){picture[i]=-1;}
	for(int i=0;i<256;i++){keylong[i]=0;keyon[i]=0;}
	cnt_FPS=0;
	gnt_FPS=start_FPS=GetNowCount();
	output_FPS=0.f;
}

//■■メイン関数■■
int ZQGameBase::GameMain(){
	if(mode==NULL){return -1;}
	else{
		while(ProcessMessage()==0 && !retourner){
			//FPS管理
			cnt_FPS++;
			now_FPS=int(start_FPS+cnt_FPS*1000.f/(FPS));
			if(now_FPS < gnt_FPS+1000/(FPS)){
				//最低限のフレーム間隔（例えば、60FPS設定なら15Fは間が無いと困る）
				now_FPS=gnt_FPS+1000/(FPS)-1;
			}
			gnt_FPS=GetNowCount();
			if(gnt_FPS<now_FPS){
				Sleep(now_FPS-gnt_FPS);
				gnt_FPS=now_FPS;
			}
			if(cnt_FPS==(FPS)){
				output_FPS=(FPS)*1000.f/(gnt_FPS-start_FPS);
				cnt_FPS=0;
				start_FPS=gnt_FPS;
			}

			//キーボード入力
			for(int i=0;i<256;i++){keyon[i]=0;}
			GetHitKeyStateAll(keyon);
			for(int i=0;i<256;i++){keyon[i]==1 ? keylong[i]++ : keylong[i]=0;}

			//実行
			mode->Run();
			ClearDrawScreen();
			//描画
			mode->Render();
			ScreenFlip();

			//モード移行処理
			if(mode->modeStatus==-1 && GetASyncLoadNum()==0){
				delete mode;
				mode=nextMode;
				nextMode=NULL;
				//Mode::toNextModeにおいてNULLを指定していれば終了
				if(mode==NULL){retourner=true;}
			}
		}
		return 0;
	}
}


/*■■■■■■■■■■■
　■■ モードクラス ■■
　■■■■■■■■■■■*/
Mode::Mode(ZQGameBase* z){
	modeCnt=0;
	modeStatus=0;
	base=z;
	for(int i=0;i<nmPICS;i++){picture[i]=-1;}
}
Mode::~Mode(){
	for(int i=0;i<nmPICS;i++){
		if(picture[i]!=-1){DeleteGraph(picture[i]);}
	}
}

//■■画像読み込み■■
int Mode::SetPic(int suffix,TCHAR* filename){
	if(0<=suffix && suffix<nmPICS && picture[suffix]==-1){
		picture[suffix]=LoadGraph(filename);
		return 0;
	}
	else{return -1;}
}

//■■複数分割画像の読み込み■■
int Mode::DivPic(int suffix,int x,int y,int w,int h,TCHAR* filename){
	for(int i=0;i<x*y;i++){
		if(picture[suffix]!=-1){return -1;}
	}
	if(0<=suffix && suffix+x*y<=nmPICS){
		LoadDivGraph(filename,x*y,x,y,w,h,&picture[suffix]);
		return 0;
	}
	else{return -1;}
}

//■■画像読み込み（モード間共有）■■
int Mode::GSetPic(int suffix,TCHAR* filename){
	if(0<=suffix && suffix<nPICS && base->picture[suffix]==-1){
		base->picture[suffix]=LoadGraph(filename);
		return 0;
	}
	else{return -1;}
}

//■■複数分割画像の読み込み（モード間共有）■■
int Mode::GDivPic(int suffix,int x,int y,int w,int h,TCHAR* filename){
	for(int i=0;i<x*y;i++){
		if(base->picture[suffix]!=-1){return -1;}
	}
	if(0<=suffix && suffix+x*y<=nPICS){
		LoadDivGraph(filename,x*y,x,y,w,h,&base->picture[suffix]);
		return 0;
	}
	else{return -1;}
}

//■■画像描画関連はGameBaseDrawing.cppへ■■

//■■次のモードを指定（NULL指定で終了）■■
void Mode::toNextMode(Mode* next){
	if(base->nextMode!=NULL){delete base->nextMode;}
	base->nextMode=next;
}

//■■次のBGMを指定（NULL指定で無音）■■
void Mode::toNextBGM(int next){
	base->nextBGM=next;
	if(next=NULL){base->nextBGM=-2;}
}
